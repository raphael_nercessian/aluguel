<div class="about-section">
   <div class="text-content">
     <div class="span7 offset1">
        @if(Session::has('success'))
          <div class="alert-box success">
          <h2>{!! Session::get('success') !!}</h2>
          </div>
        @endif
        <div class="secure">Upload form</div>
        {!! Form::open(array('url'=>'apply/upload','method'=>'POST', 'files'=>true)) !!}
         <div class="control-group">
          <div class="controls">
          {!! Form::file('image') !!}
      <p class="errors">{!!$errors->first('image')!!}</p>
    @if(Session::has('error'))
    <p class="errors">{!! Session::get('error') !!}</p>
    @endif
        </div>
        </div>
        <div id="success"> </div>
      {!! Form::submit('Submit', array('class'=>'send-btn')) !!}
      {!! Form::close() !!}
      </div>
   </div>
</div>

<!--
                                <h1>File Upload</h1>
                                <form action="{{ URL::to('upload') }}" method="post" enctype="multipart/form-data">
                                        <label>Select image to upload:</label>
                                    <input type="file" name="file" id="file">
                                    <input type="submit" value="Upload" name="submit">
                                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                </form>
-->