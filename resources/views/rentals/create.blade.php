@extends('layouts.app')

@section('content')
<div class="container">
<style>
.form-group {
    padding: 10px;
    clear: both;
    margin-bottom: 15px;
}
</style>
    <h1>Anunciar imóvel para aluguel</h1>
    <hr/>
{!! Form::open(
    array(
        'route' => 'rentals.store', 
        'class' => 'form', 
        'novalidate' => 'novalidate', 
        'files' => true)) !!}

                <div class="form-group {{ $errors->has('titulo') ? 'has-error' : ''}}">
                {!! Form::label('titulo', trans('rentals.titulo'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('titulo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('preco') ? 'has-error' : ''}}">
                {!! Form::label('preco', trans('rentals.preco'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('preco', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('preco', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('endereco') ? 'has-error' : ''}}">
                {!! Form::label('endereco', trans('rentals.endereco'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('endereco', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <a href="javascript:void()" id="procura"><span style="background-color:yellow;padding:10px">Adicionar Mapa</span></a>
          
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                    <span class="erro_msg"></span>
                    <input type="hidden" name="location" id="location">
                    <script>
                    $(document).ready(function(){
                     
                     $('#procura').on('click', function(){
                                    
                                    var address = $('#endereco').val() + ' , Brasil';
                             
                               // alert(address);
                                var key = 'AIzaSyB70QW12xlmQyAThpy1VwVjpOgqMD-s698';
                                    $.get('https://maps.googleapis.com/maps/api/geocode/json', { 'address': address,'sensor':false },
                                      function(data){
                                         $('#erro_msg').val(data);
                     
                                              if(data.results.length){
                            //    $('#latitude').val(data.results[0].geometry.location.lat);
                         //   $('#longitude').val(data.results[0].geometry.location.lng);
                         $('#location').val( (data.results[0].geometry.location.lat) +',' +  (data.results[0].geometry.location.lng) );
                                alert('O endereço foi encontrado');
                            }else{
                                alert('Endereço Inválido');
                           }
                        });
                     
                        });
                    });
                    </script>

            <div class="form-group">
                {!! Form::label('Image') !!}
                {!! Form::file('image', null) !!}
            </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection