@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Edit Rental {{ $rental->id }}</h1>

    {!! Form::model($rental, [
        'method' => 'PATCH',
        'url' => ['/rentals', $rental->id],
        'class' => 'form-horizontal'
    ]) !!}

                                <h1>File Upload</h1>
                                <form action="{{ URL::to('upload') }}" method="post" enctype="multipart/form-data">
                                        <label>Select image to upload:</label>
                                    <input type="file" name="file" id="file">
                                    <input type="hidden" name="id" value="id">
                                    <input type="submit" value="Upload" name="submit">
                                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                </form>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection