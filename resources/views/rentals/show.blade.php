@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Rental {{ $rental->id }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID.</th><td>{{ $rental->id }}</td>
                </tr>
                <tr><th> {{ trans('rentals.titulo') }} </th><td> {{ $rental->titulo }} </td></tr><tr><th> {{ trans('rentals.preco') }} </th><td> {{ $rental->preco }} </td></tr><tr><th> {{ trans('rentals.endereco') }} </th><td> {{ $rental->endereco }} </td></tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <a href="{{ url('rentals/' . $rental->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Rental"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['rentals', $rental->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Rental',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
@endsection