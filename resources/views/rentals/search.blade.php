@extends('layouts.app')

@section('content')
<div class="container">
                <title>Encontre um imóvel</title>

                <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

                <style>
                       

                        .quote {
                                font-size: 24px;
                        }

                        label{
                                margin-right:20px;
                        }

                        form{
                                background:#f5f5f5;
                                padding:20px;
                                border-radius:10px;
                        }

                        input[type="submit"]{
                                background:#0098cb;
                                border:0px;
                                border-radius:5px;
                                color:#fff;
                                padding:10px;
                                margin:20px auto;
                        }
                        #endereco{
                            width: 400px;
                        }

                </style>
        </head>
        <body>
                <div class="container">
                        <div class="content">
                        <form method="get" id="target" action="/search">      
                            <input id="endereco" name="endereco" type="text">                     
                            <input class="picker-search-button" type="button" id="procura" value="Pesquisar">
                            <input type="hidden" name="coord" id="location">
                        </form>  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
     
     $('#procura').on('click', function(){
                    
                    var address = $('#endereco').val() + ' , Brasil';
                    var key = 'AIzaSyB70QW12xlmQyAThpy1VwVjpOgqMD-s698';
                    $.get('https://maps.googleapis.com/maps/api/geocode/json', { 'address': address,'sensor':false },
                      function(data){
                         $('#erro_msg').val(data);
                        if(data.results.length){
     
            $('#location').val( (data.results[0].geometry.location.lat) +' ' +  (data.results[0].geometry.location.lng) );
                     $( "#target" ).submit();
            }else{
               
           }
        });
     
        });
    });
    </script>

  
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Foto</th><th> Título </th><th> Valor </th><th> Endereço </th><?php if(isset($_GET['coord'])){ ?><th>Distância (em Km)</th><?php } ?>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($rentals as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td><?php if($item->image){ echo "<img src='/uploads/".$item->id.'-file.'.$item->image."' style='width:100px'>"; } ?></td>
                    <td>{{ $item->titulo }}</td><td>{{ $item->preco }}</td><td>{{ $item->endereco }}</td><?php if(isset($_GET['coord'])){ ?><td>{{ round($item->distance) }} KM </td><?php } ?>
            
                </tr>
            @endforeach
            </tbody>
        </table>
     
    </div>

</div>
@endsection
