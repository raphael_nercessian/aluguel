@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Rentals <a href="{{ url('/rentals/create') }}" class="btn btn-primary btn-xs" title="Add New Rental"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> {{ trans('rentals.titulo') }} </th><th> {{ trans('rentals.preco') }} </th><th> {{ trans('rentals.endereco') }} </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($rentals as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->titulo }}</td><td>{{ $item->preco }}</td><td>{{ $item->endereco }}</td>
                    <td>
                        <a href="{{ url('/rentals/' . $item->id) }}" class="btn btn-success btn-xs" title="View Rental"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/rentals/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Rental"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/rentals', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Rental" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Rental',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $rentals->render() !!} </div>
    </div>

</div>
@endsection
