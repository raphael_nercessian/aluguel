<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRentalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rentals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('endereco');
			$table->string('image');
			$table->string('preco');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
		  DB::statement('ALTER TABLE items ADD location POINT' );
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rentals');
	}

}
