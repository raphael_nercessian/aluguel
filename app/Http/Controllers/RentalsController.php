<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Rental;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class RentalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
 
        $rentals = Rental::paginate(15);

     
        return view('rentals.index', compact('rentals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('rentals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
      /*
        Rental::create($request->all());

        Session::flash('flash_message', 'Rental added!');
        $id = DB::getPdo()->lastInsertId();  
        //return redirect('rentals');

        $rental = Rental::findOrFail($id);

        return view('rentals.edit2', compact('rental'));

        */
$ext=$request->file('image')->getClientOriginalExtension();

        $rental = new Rental(array(
          'titulo' => $request->get('titulo'),
          'endereco' => $request->get('endereco'),
          'location' => $request->get('location'),
          'preco'  => $request->get('preco'),
          'image' =>  $ext
        ));

    $rental->save();

    $imageName = $rental->id . '-file.' . 
        $request->file('image')->getClientOriginalExtension();

    $request->file('image')->move(
        base_path() . '/public/uploads/', $imageName
    );

   // return \Redirect::route('rentals.edit', 
     //   array($rental->id))->with('message', 'Imóvel adicionado!');           
        Session::flash('flash_message', 'Rental updated!');

        return redirect('rentals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $rental = Rental::findOrFail($id);

        return view('rentals.show', compact('rental'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $rental = Rental::findOrFail($id);

        return view('rentals.edit', compact('rental'));
    }


    public function edit2($id)
    {
        $rental = Rental::findOrFail($id);

        return view('rentals.edit', compact('rental'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $rental = Rental::findOrFail($id);
        $rental->update($request->all());

        Session::flash('flash_message', 'Rental updated!');

        return redirect('rentals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Rental::destroy($id);

        Session::flash('flash_message', 'Rental deleted!');

        return redirect('rentals');
    }

    public function search()
    {

        if(isset($_GET['coord'])){
            $rentals = DB::select("SELECT
                                    titulo, preco, endereco, image, id,
                                    (GLength(
                                    LineStringFromWKB(
                                      LineString(
                                        location, 
                                        GeomFromText('POINT(".$_GET['coord'].")')
                                      )
                                     )
                                    )) * 100
                                    AS distance
                                    FROM rentals
                                    ORDER BY distance ASC");
 

             return view('rentals.search', compact('rentals'));
        } else {

        $rentals = Rental::paginate(15);
        return view('rentals.search', compact('rentals'));
        }
    }






}
