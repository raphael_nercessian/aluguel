<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('welcome');

});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('cars', 'CarsController');

Route::get('map','ItemsController@map');

Route::get('map/create','ItemsController@create');

Route::get('map/lista','ItemsController@lista');


Route::post('upload', 'UploadController@upload');


Route::get('media', [ 'as' => 'image.create', 'uses' => 'ImageController@create']);
Route::post('media', [ 'as' => 'image.store', 'uses' => 'ImageController@save']);
Route::get('media/show', [ 'as' => 'image.resized', 'uses' => 'ImageController@show']);

Route::resource('rentals', 'RentalsController');
Route::resource('/search', 'RentalsController@search');

 